package com.LAB4.atividade_4.Services;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.LAB4.atividade_4.entities.Produto;
import com.LAB4.atividade_4.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProdutoService {
    @Autowired
    private ProdutoRepository produtoRepository;

    public List<Produto> listarProdutos() {
        return produtoRepository.findAll();
    }

    public Produto obterProdutoPorId(UUID id) {
        return produtoRepository.findById(id).orElse(null);
    }

    public void adicionarProduto(Produto produto) {
        if (produto.getEstoque() < 0 || produto.getPreco() < 0) {
            throw new RuntimeException("O estoque e o preço do produto não podem ser negativos.");
        }
        produtoRepository.save(produto);
    }

    public void atualizarProduto(UUID id, Produto produto) {
        Optional<Produto> produtoOptional = produtoRepository.findById(id);
        if (produtoOptional.isPresent()) {
            Produto produtoExistente = produtoOptional.get();
            produtoExistente.setNome(produto.getNome());
            produtoExistente.setPreco(produto.getPreco());
            produtoExistente.setEstoque(produto.getEstoque());
            produtoRepository.save(produtoExistente);
        } else {
            throw new RuntimeException("Produto não encontrado");
        }
    }

    public void excluirProduto(UUID id) {
        produtoRepository.deleteById(id);
    }
}
