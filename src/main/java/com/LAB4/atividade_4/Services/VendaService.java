package com.LAB4.atividade_4.Services;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.LAB4.atividade_4.entities.Produto;
import com.LAB4.atividade_4.entities.Venda;
import com.LAB4.atividade_4.repositories.ProdutoRepository;
import com.LAB4.atividade_4.repositories.VendaRepository;
import com.LAB4.atividade_4.request.VendaRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class VendaService {
    @Autowired
    private VendaRepository vendaRepository;

    @Autowired
    private ProdutoRepository produtoRepository;

    public List<Venda> historicoVendas() {
        return vendaRepository.findAll();
    }

    public Venda obterVendaPorId(UUID id) {
        return vendaRepository.findById(id).orElse(null);
    }


    public void realizarVenda(VendaRequest vendaRequest) {
        UUID idProduto = vendaRequest.getIdProduto();
        Produto produto = produtoRepository.findById(idProduto).orElse(null);
        if (produto != null && produto.getEstoque() >= vendaRequest.getQuantidade()) {
            if (vendaRequest.getQuantidade() <= 0) {
                throw new RuntimeException("A quantidade de produtos vendidos deve ser maior que zero.");
            }

            double precoProduto = produto.getPreco();
            double precoTotal = precoProduto * vendaRequest.getQuantidade();
            if (precoTotal < 0) {
                throw new RuntimeException("O preço total da venda não pode ser negativo.");
            }

            produto.setEstoque(produto.getEstoque() - vendaRequest.getQuantidade());
            produtoRepository.save(produto);

            Venda venda = new Venda();
            venda.setProduto(produto);
            venda.setQuantidade(vendaRequest.getQuantidade());
            venda.setPrecoTotal(precoTotal);
            venda.setData(new Date());
            vendaRepository.save(venda);
        } else {
            throw new RuntimeException("Produto não encontrado ou estoque insuficiente.");
        }
    }

    public void atualizarVenda(UUID id, VendaRequest vendaRequest) {
        Optional<Venda> vendaOptional = vendaRepository.findById(id);
        if (vendaOptional.isPresent()) {

            Venda venda = vendaOptional.get();
            vendaRepository.save(venda);
        } else {
            throw new RuntimeException("Venda não encontrada");
        }
    }

    public void excluirVenda(UUID id) {
        vendaRepository.deleteById(id);
    }
}


