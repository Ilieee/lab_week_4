package com.LAB4.atividade_4.entities;

import java.util.Date;
import java.util.UUID;

import jakarta.persistence.*;


@Entity
@Table(name = "vendas")
public class Venda {
    @Id
    @GeneratedValue
    @Column(name = "id_venda")
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "id_produto")
    private Produto produto;

    @Column(name = "quantidade")
    private int quantidade;

    @Column(name = "data")
    private Date data;

    @Column(name = "preco_total")
    private double precoTotal;



    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public double getPrecoTotal() {
        return precoTotal;
    }

    public void setPrecoTotal(double precoTotal) {
        this.precoTotal = precoTotal;
    }
}

