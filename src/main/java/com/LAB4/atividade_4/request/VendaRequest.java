package com.LAB4.atividade_4.request;

import java.util.UUID;

public class VendaRequest {

    private UUID idProduto;
    private int quantidade;

    public UUID getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(UUID idProduto) {
        this.idProduto = idProduto;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
}
