package com.LAB4.atividade_4.repositories;

import com.LAB4.atividade_4.entities.Venda;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.UUID;


@Repository
public interface VendaRepository extends JpaRepository<Venda, UUID> {
}
