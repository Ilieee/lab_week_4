package com.LAB4.atividade_4.Controllers;

import com.LAB4.atividade_4.Services.VendaService;
import com.LAB4.atividade_4.entities.Venda;
import com.LAB4.atividade_4.request.VendaRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/vendas")
public class VendaController {
    @Autowired
    private VendaService vendaService;

    @GetMapping
    public List<Venda> historicoVendas() {
        return vendaService.historicoVendas();
    }

    @GetMapping("/{id}")
    public Venda detalhesVenda(@PathVariable UUID id) {
        return vendaService.obterVendaPorId(id);
    }

    @PostMapping
    public void realizarVenda(@RequestBody VendaRequest vendaRequest) {
        vendaService.realizarVenda(vendaRequest);
    }

    @PutMapping("/{id}")
    public void atualizarVenda(@PathVariable UUID id, @RequestBody VendaRequest vendaRequest) {
        vendaService.atualizarVenda(id, vendaRequest);
    }

    @DeleteMapping("/{id}")
    public void excluirVenda(@PathVariable UUID id) {
        vendaService.excluirVenda(id);
    }
}
