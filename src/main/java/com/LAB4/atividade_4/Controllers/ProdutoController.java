package com.LAB4.atividade_4.Controllers;

import com.LAB4.atividade_4.Services.ProdutoService;
import com.LAB4.atividade_4.entities.Produto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {
    @Autowired
    private ProdutoService produtoService;

    @GetMapping
    public List<Produto> listarProdutos() {
        return produtoService.listarProdutos();
    }

    @GetMapping("/{id}")
    public Produto detalhesProduto(@PathVariable UUID id) {
        return produtoService.obterProdutoPorId(id);
    }

    @PostMapping
    public void adicionarProduto(@RequestBody Produto produto) {
        produtoService.adicionarProduto(produto);
    }

    @DeleteMapping("/{id}")
    public void excluirProduto(@PathVariable UUID id) {
        produtoService.excluirProduto(id);
    }
    @PutMapping("/{id}")
    public void atualizarProduto(@PathVariable UUID id, @RequestBody Produto produto) {
        produtoService.atualizarProduto(id, produto);
    }
}
