# Documentação da API de Vendas de Produtos

Esta é uma API RESTful desenvolvida para lidar com vendas de produtos em uma loja virtual.

## Endpoints Disponíveis

### Listar Produtos Disponíveis

**Endpoint:** `GET /produtos`

**Descrição:** Retorna uma lista de todos os produtos disponíveis para venda, incluindo o nome, preço e quantidade em estoque de cada produto.

### Detalhes de um Produto

**Endpoint:** `GET /produtos/{id}`

**Descrição:** Retorna os detalhes de um produto específico com base no seu ID, incluindo nome, preço e quantidade em estoque.

### Realizar uma Venda

**Endpoint:** `POST /vendas`

**Descrição:** Permite que um cliente realize uma venda. O corpo da solicitação deve incluir o ID do produto a ser vendido e a quantidade desejada. Após a venda ser concluída com sucesso, o estoque do produto deve ser atualizado.

### Histórico de Vendas

**Endpoint:** `GET /vendas`

**Descrição:** Retorna o histórico de todas as vendas realizadas, incluindo detalhes como o ID da venda, ID do produto vendido, quantidade e data da venda.

## Regras de Negócio

- Um produto só pode ser vendido se houver estoque disponível.
- O preço do produto não pode ser negativo.
- A quantidade vendida em uma única transação não pode ser negativa ou zero.
- A data da venda é registrada automaticamente como a data atual.
- Implementação de descontos progressivos com base na quantidade de produtos comprados. Por exemplo, ofereça um desconto de 5% para compras de mais de 10 unidades de um produto e um desconto de 10% para compras de mais de 20 unidades.
